﻿# Define the primary and foreign keys
$primaryKey = 'ec5_uuid'
$foreignKey = 'ec5_branch_owner_uuid'

try {
    Write-Host "Starting processing..."

    # Import the primary CSV file
    Write-Host "Importing primary CSV file..."
    $primaryCsv = Import-Csv -Path '.\form-1__nitamaria-purse-seining.csv'
    if ($primaryKey -notin $primaryCsv[0].PSObject.Properties.Name) {
        throw "Primary key '$primaryKey' not found in primary CSV file."
    }
    Write-Host "Primary key '$primaryKey' found in primary CSV file."

    # Get all other CSV files in the current directory, excluding lock files
    Write-Host "Getting other CSV files..."
    $otherCsvFiles = Get-ChildItem -Path '.' -Filter '*.csv' | Where-Object { $_.Name -ne 'form-1__nitamaria-purse-seining.csv' -and $_.Name -notlike '*.lock'  -and $_.Name -ne 'joined-files.csv' }

    # Perform the JOIN operation
    Write-Host "Performing JOIN operation..."
    foreach ($csvFile in $otherCsvFiles) {
        $otherCsv = Import-Csv -Path $csvFile.FullName
        if ($foreignKey -notin $otherCsv[0].PSObject.Properties.Name) {
            throw "Foreign key '$foreignKey' not found in CSV file '$csvFile'."
        }
        Write-Host "Foreign key '$foreignKey' found in CSV file '$csvFile'."
        $primaryCsv = $primaryCsv | ForEach-Object {
            $primaryRow = $_
            $matchingRows = $otherCsv | Where-Object { $_.$foreignKey -eq $primaryRow.$primaryKey }
            if ($matchingRows) {
                foreach ($matchingRow in $matchingRows) {
                    $newRow = $primaryRow.PSObject.Copy()
                    foreach ($property in $matchingRow.PSObject.Properties) {
                        $newRow | Add-Member -NotePropertyName ($property.Name + $csvFile.Name) -NotePropertyValue $property.Value
                    }
                    $newRow
                }
            } else {
                $primaryRow
            }
        }
    }

    # Export the result to a new CSV file
    Write-Host "Exporting result to a new CSV file..."
    $primaryCsv | Export-Csv -Path '.\joined-files.csv' -NoTypeInformation

    Write-Host "Processing completed successfully."
}
catch {
    # If an error occurs, output it to the console
    Write-Host "An error occurred during processing: $_"
}

# Wait for the user to press a key before closing
Write-Host "Press enter key to exit..."
$null = Read-Host
